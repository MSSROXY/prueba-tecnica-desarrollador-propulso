import React from "react";
import { useNavigate } from "react-router-dom";
import placeholder from "../assets/placeholder.png";
import "../style/Home.css";

const Home = () => {
  const navigate = useNavigate();

  const clickSeries = () => {
    navigate("/series");
  };

  const clickMovies = () => {
    navigate("/movies");
  };

  return (
    <section className="container">
      <div className="div-series">
        <button onClick={clickSeries}>
          <img src={placeholder} alt="Series"></img>
          <div className="div-text">SERIES</div>
        </button>
        <p>Popular Series</p>
      </div>
      <div className="div-movies">
        <button onClick={clickMovies}>
          <img src={placeholder} alt="Movies"></img>
          <div className="div-text">MOVIES</div>
        </button>
        <p>Popular Movies</p>
      </div>
    </section>
  );
};

export default Home;
