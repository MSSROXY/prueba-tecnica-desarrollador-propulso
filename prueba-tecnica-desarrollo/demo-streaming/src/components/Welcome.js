import React from "react";
import { useNavigate } from "react-router-dom";
import "../style/Welcome.css";

const Welcome = () => {
  const navigate = useNavigate();

  const clickHome = () => {
    navigate("/");
  };

  return (
    <header className="welcome">
      <section className="welcome-nav">
        <h1 onClick={clickHome} >DEMO Streaming</h1>
        <div>
          <button>Log in</button>
          <button className="btn-trial">Start your free trial</button>
        </div>
      </section>
      <section className="welcome-title">
        <h4>Popular Titles</h4>
      </section>
    </header>
  );
};

export default Welcome;
