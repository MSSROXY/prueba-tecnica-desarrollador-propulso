import React from "react";
import { useNavigate } from "react-router-dom";
import facebook from "../assets/social/facebook-white.svg";
import twitter from "../assets/social/twitter-white.svg";
import instagram from "../assets/social/instagram-white.svg";
import appStore from "../assets/store/app-store.svg";
import playStore from "../assets/store/play-store.svg";
import windowStore from "../assets/store/windows-store.svg";
import "../style/Footer.css"

const Footer = () => {
  const navigate = useNavigate();

  const clickHome = () => {
    navigate("/");
  };
  return (
    <footer>
      <div className="footer-options">
          <p onClick={clickHome}>Home</p>|<p>Terms and Conditions</p>|
          <p>Privacy Policy</p>|<p>Collection Statement</p>|<p>Help</p>|
          <p>Manage Account</p>
      </div>
      <div className="footer-options">
        <p>Copyright © 2016 DEMO Streaming All Rights Reserved</p>
      </div>
      <div className="footer-networks">
        <div className="social-media">
          <img src={facebook} alt="facebook" />
          <img src={twitter} alt="twitter" />
          <img src={instagram} alt="instagram" />
        </div>
        <div className="store">
          <img src={appStore} alt="app-store" />
          <img src={playStore} alt="play-store" />
          <img src={windowStore} alt="windows-store" />
        </div>
      </div>
    </footer>
  );
};

export default Footer;
