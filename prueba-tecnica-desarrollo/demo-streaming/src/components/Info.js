import React from "react";
import "../style/Info.css";

const Info = ({ infoTitle, setShowModal }) => {
  const closeModal = () => {
    setShowModal(false);
  };

  return (
    <div className="modal-background">
      <div className="container-modal">
        <div className="info">
          <h3>{infoTitle.title}</h3>
          <h3>{infoTitle.releaseYear}</h3>
          <p>{infoTitle.description}</p>
          <button onClick={closeModal}> OK </button>
        </div>
        <img className="img"
          src={infoTitle.images["Poster Art"].url}
          alt={infoTitle.title}
        ></img>
      </div>
    </div>
  );
};

export default Info;
