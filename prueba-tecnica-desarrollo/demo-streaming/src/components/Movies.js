import { useEffect, useState } from "react";
import { fetchAllTitles } from "../redux/slice";
import { useDispatch, useSelector } from "react-redux";
import "../style/Series.css";
import Info from "./Info";

const Movies = () => {
  const [showModal, setShowModal] = useState(false);
  const [infoTitle, setInfoTitle] = useState("");
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchAllTitles());
  }, [dispatch]);

  const { list: titles } = useSelector((state) => {
    return state.titleReducer;
  });

  const showInfo = (title) => {
    setInfoTitle(title);
    setShowModal(true);
  };

  const myMovies = titles
    .filter(
      (title) => title.programType === "movie" && title.releaseYear >= 2010
    )
    .slice(1, 21)
    .sort(function (a, b) {
      return a.title < b.title ? -1 : 1;
    });

  return (
    <div className="container">
      {myMovies.map((title, index) => (
        <article key={index} className="img-container">
          <button onClick={() => showInfo(title)}>
            <img src={title.images["Poster Art"].url} alt={title.title} />
            <p>{title.title}</p>
          </button>
          {showModal ? (
            <Info infoTitle={infoTitle} setShowModal={setShowModal} />
          ) : (
            false
          )}
        </article>
      ))}
    </div>
  );
};

export default Movies;
