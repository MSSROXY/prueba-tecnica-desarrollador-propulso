import React from "react";
import Footer from "./components/Footer";
import Welcome from "./components/Welcome";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "./components/Home";
import Series from "./components/Series";
import Movies from "./components/Movies";
import "./App.css";
import { Provider } from "react-redux";
import store from "./redux/store";

const App = () => {
  return (
    <Provider store={store}>
      <div className="app-container">
        <Router>
          <Welcome />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/movies" element={<Movies />} />
            <Route path="/series" element={<Series />} />
          </Routes>
          <Footer />
        </Router>
      </div>
    </Provider>
  );
};

export default App;
