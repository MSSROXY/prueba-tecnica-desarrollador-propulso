import { configureStore } from "@reduxjs/toolkit";
//reducerss:
import { titleReducer } from "./slice";

export default configureStore({
  reducer: {
    titleReducer,
  },
});
