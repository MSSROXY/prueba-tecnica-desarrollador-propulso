import { createSlice } from "@reduxjs/toolkit";
// import data from '../data/sample.json'

export const titleSlice = createSlice({
  name: "titles",
  initialState: {
    list: [],
  },
  reducers: {
    setTitleList: (state, action) => {
      state.list = action.payload;
    },
  },
});
export const { setTitleList } = titleSlice.actions;
export const titleReducer = titleSlice.reducer;

export const fetchAllTitles = () => {
  return (dispatch) => {
    fetch(
      "https://raw.githubusercontent.com/StreamCo/react-coding-challenge/master/feed/sample.json"
    )
      .then((res) => res.json())
      .then((response) => {
        dispatch(setTitleList(response.entries));
      })
      .catch((error) => console.log(error));
  };
};
