# Prueba técnica — Desarrollador Propulso

## Instrucciones

- Luego de clonar el proyecto, tiene que ingresar a la carpeta de `demo-streaming` que contiene el proyecto
- En la terminal ejecutar `npm start`
- Por defecto la app se ejecutará en el puerto 3000.

## Consideraciones

- ¿Cómo decidió las opciones técnicas y arquitectónicas utilizadas como parte de su solución?
A partir de los requerimientos solicitados, se decidieron las herramientas/librerías a utilizar. Por la estructura del proyecto creo que el uso de Redux no era necesario, sin embargo aprendí el uso de sus funciones para poder hacer uso del "estado global", (usar props o useContext podría haber sido una solución igual de eficaz). Reconozco que un error fue no tomar en consideración que las imágenes eran solo referenciales y la app se construyó tal cual se ven en las fotos usando CSS modules. 

- ¿Hay alguna mejora que pueda hacer en su envío?
Las funciones no están al 100%, la primera versión aún no filtra por tipo o cantidad. Los componenetes Movies/Series se pueden simplificar usando uno solo y aplicando filtro solo por "programType".

- ¿Qué haría de manera diferente si se le asignara más tiempo?
En un futuro se espera implementar los filtros requeridos, el estado de "carga/error", el uso de TypeScript y posiblemente agregar el registro/login de usuarios. 

